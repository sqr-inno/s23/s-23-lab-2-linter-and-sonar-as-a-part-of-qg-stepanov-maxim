
[![pipeline status](https://gitlab.com/sqr-inno/s23/s-23-lab-2-linter-and-sonar-as-a-part-of-qg-stepanov-maxim/badges/master/pipeline.svg)](https://gitlab.com/sqr-inno/s23/s-23-lab-2-linter-and-sonar-as-a-part-of-qg-stepanov-maxim/-/commits/master)
# Lab 2 -- Linter and SonarQube as a part of quality gates

I had to switch to 11 version of Java in `pom.xml` for local and SonarCloud running.

Shared runner took to long to pick up my jobs, so here are screenshots of local SonarQube execution:

![](/images/SonarQube1.png)
![](/images/SonarQube2.png)